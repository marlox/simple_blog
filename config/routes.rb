Rails.application.routes.draw do
	# User
	resources :users, only: [:index, :new, :create]
	resources :user_sessions, only: [:create, :destroy]
	resources :password_resets, only: [:new, :create, :edit, :update]
	delete "/sign_out", to: "user_sessions#destroy", as: :sign_out
	get "/sign_in", to: "user_sessions#new", as: :sign_in
	#/User

	# Blog
	post "/blog/all/search", to: "blog#all", as: "search_all"
	get "/blog/all/search", to: "blog#all"
	get "/blog/all", to: "blog#all", as: "all_blog"
	get "/blog/all/:id/read", to: "blog#read", as: "read_blog"
	
	resources :blog do
		resources :comments
	end
	
	post "/blog/search", to: "blog#index", as: "search_blog"
	get "/blog", to: "blog#index", as: "blags"
	get "/blog/:id", to: "blog#update", as: "blag"
	get "/blog", to: "blog#index"
	get "/blog/:id/edit", to: "blog#edit", as: "edit_blag"
	
	get "/blog/:blag_id/comments", to: "comments#index", as: "blag_comments"
	post "/blog/:blag_id/comments", to: "comments#create"
	get "/blog/:blag_id/comments/new", to: "comments#new", as: "new_blag_comment"
	get "/blog/:blag_id/comments/:id/edit", to: "comments#edit", as: "edit_blag_comment"
	get "/blog/:blag_id/comments/:id", to: "comments#show", as: "blag_comment"
	patch "/blog/:blag_id/comments/:id", to: "comments#update"
	put "/blog/:blag_id/comments/:id", to: "comments#update"
	delete "/blog/:blag_id/comments/:id", to: "comments#destroy"
	#/Blog

	# Blog API
	namespace :api, path: "/v1", defaults: { format: :json, constraints: { subdomain: "api" } } do
		resources :blog, only: [:index, :show] do
			resources :comments
		end
	end
	#/Blog API
	
	root "welcome#index"
end