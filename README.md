# SIMPLE BLOG

An application for an individual that will serve as personal online diary wherein thoughts and feelings can be freely expressed through words.
 
 
## Main Features
 
### 1. Owner
* Add Articles
* Edit Articles
* Remove Articles
* Read Comments
* Remove own Comment
 
### 2. Guests
* Read Articles
* Read Comments
* Remove own Comment
  
 
## Specifications
* Ruby Version: 2
* Rails Version: 4
* Database: PostgreSQL
