class CreateComments < ActiveRecord::Migration
	def change
		create_table :comments do |t|
			t.string :commentor
			t.text :remarks
			t.references :blag, index: true, foreign_key: true
			
			t.timestamps null: false
		end
	end
end
