class CreateBlags < ActiveRecord::Migration
	def change
		create_table :blags do |t|
			t.string :title
			t.text :contents
      t.references :user, index: true, foreign_key: true
      
			t.timestamps null: false
		end
	end
end
