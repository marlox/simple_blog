class Blag < ActiveRecord::Base
	belongs_to :user

	has_many :comments, dependent: :destroy
	
	scope :blogs, -> { joins(:user).select("blags.*, users.email author") }
	scope :created_asc, -> { order("created_at") }
	scope :created_desc, -> { order("created_at desc") }
	scope :search_blog, -> (search, all = false) { where("LOWER(title) LIKE LOWER(:search) OR LOWER(contents) LIKE LOWER(:search)".to_s() + (all ? "OR LOWER(users.email) LIKE LOWER(:search)" : "").to_s(), { search: "%#{search}%" }) }
	
	validates :title, presence: true, length: { minimum: 5 }, uniqueness: { scope: :user_id, message: "already exists." }, allow_blank: false
	
	validates :contents, presence: true, allow_blank: false
	
	before_save { |blog| blog.title = blog.title.titleize }
end