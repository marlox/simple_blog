class UserSession < Authlogic::Session::Base
	consecutive_failed_logins_limit 5
	failed_login_ban_for 0
	logout_on_timeout true
end