class User < ActiveRecord::Base
	has_many :blags, dependent: :destroy

	acts_as_authentic do |c|
		c.crypto_provider = Authlogic::CryptoProviders::Sha512
		c.login_field = :email
		c.logged_in_timeout = 10.hours
	end
	
	validates :email, presence: true, allow_blank: false
	
	validates :password, presence: true, allow_blank: false
	
	before_save { |user| user.email.downcase! }
	
	def deliver_password_reset_instructions!
		reset_perishable_token!
		PasswordResetMailer.reset_email(self).deliver_now
	end
end