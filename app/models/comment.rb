class Comment < ActiveRecord::Base
	belongs_to :blag
	
	validates :commentor, presence: true, length: { minimum: 3 }
	
	validates :remarks, presence: true, allow_blank: false
end