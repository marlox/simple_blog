class PasswordResetsController < ApplicationController
	skip_before_action :require_login
	
	private def password_reset_params
		params.require(:user).permit(:password, :password_confirmation)
	end
	
	def new
	end
	
	def create
		@user = User.find_by_email(params[:email])
		email = params[:email]
		email = " <strong>"+ email +"</strong>" if !email.blank?
		if !@user
			respond_to do |format|
				flash[:danger] = "Unable to continue forgot password because email"+ email +" not found."
				format.html { redirect_to new_password_reset_path and return }
				format.json { render json: flash[:danger], status: :unprocessable_entity }
			end
		end
		respond_to do |format|
			if @user.deliver_password_reset_instructions!
				flash[:success] = "Instructions to reset your password has been sent to"+ email +". If you can find it in your Inbox, kindly check it in your Spam or Junk folder. Thank you!"
				format.html { redirect_to root_path }
				format.json { render :show, status: :created, location: root_path }
			else
				flash[:danger] = "Unable to continue forgot password for"+ email +" because "+ @user.errors.to_a.join(", ") +"."
				format.html { redirect_to new_password_reset_path }
				format.json { render json: @user.errors, status: :unprocessable_entity }
			end
		end
	end
	
	def edit
		@user = User.find_by(perishable_token: params[:id])
		if !@user
			respond_to do |format|
				flash[:danger] = "Unable to reset password due to invalid token."
				format.html { render :edit }
				format.json { render json: flash[:danger], status: :unprocessable_entity }
			end
		end
	end
	
	def update
		@user = User.find_by(perishable_token: params[:id], email: params[:email])
		if !@user
			respond_to do |format|
				flash[:danger] = "Unable to reset password because token and email <strong>"+ params[:email].to_s() +"</strong> not matched."
				format.html { redirect_to edit_password_reset_path(params[:id]) and return }
				format.json { render json: flash[:danger], status: :unprocessable_entity }
			end
		end
		
		transact = @user.update_attributes(password_reset_params)
		email = @user.email.to_s()
		email = " <strong>"+ email +"</strong>" if !email.blank?
		respond_to do |format|
			if transact
				flash[:success] = "Reset Password for"+ email +" successful."
				format.html { redirect_to root_path }
				format.json { render :show, status: :ok, location: root_path }
				current_user_session.destroy
			else
				flash[:danger] = "Unable to reset password for"+ email +" because "+ @user.errors.to_a.join(", ") +"."
				format.html { render :edit }
				format.json { render json: @user.errors, status: :unprocessable_entity }
			end
		end
	end
end