class BlogController < ApplicationController
	before_filter :get_user, except: [ :all, :read ]

	private def blog_params
		params.require(:blag).permit(:title, :contents) unless params[:search]
	end
	
	def get_user
		@user = User.find(current_user)
	end

	def index
		search = params[:search]
		if search
			@blog = @user.blags.search_blog(search).created_asc
		else
			@blog = @user.blags.created_asc
		end
		if @blog.blank?
			respond_to do |format|
				flash[:danger] = (search ? "No blog with <strong>"+ params[:search].to_s() +"</strong> found" : "No blog to be searched yet! Get started &raquo; "+ '<a href="'+ new_blog_path.to_s() +'" title="Create New" data-toggle="tooltip" data-placement="bottom"><span class="glyphicon glyphicon-plus-sign"></span><span class="sr-only">Create</span></a>')
				format.html # index.html.erb
				format.json { render json: flash[:danger], status: :unprocessable_entity }
			end
			@no_blog_found = flash[:danger]
		end
		flash[:success] = "<u>"+ @blog.length.to_s() +"</u> blog".pluralize(@blog.length) +" found with <strong>"+ params[:search].to_s() +"</strong>" if search && @blog.length > 0
		@blog = @blog.reverse
	end
	
	def show
		@blog = @user.blags.find(params[:id])
	end
	
	def all
		search = params[:search]
		if search
			@blog = Blag.blogs.search_blog(search, true).created_asc
		else
			@blog = Blag.blogs.created_asc
		end
		if @blog.blank?
			respond_to do |format|
				flash[:danger] = (search ? "No blog with <strong>"+ params[:search].to_s() +"</strong> found" : "No blog to be searched yet! Get started &raquo; "+ '<a href="'+ new_blog_path.to_s() +'" title="Create New" data-toggle="tooltip" data-placement="bottom"><span class="glyphicon glyphicon-plus-sign"></span><span class="sr-only">Create</span></a>')
				format.html # index.html.erb
				format.json { render json: flash[:danger], status: :unprocessable_entity }
			end
			@no_blog_found = flash[:danger]
		end
		flash[:success] = "<u>"+ @blog.length.to_s() +"</u> blog".pluralize(@blog.length) +" found with <strong>"+ params[:search].to_s() +"</strong>" if search && @blog.length > 0
		@blog = @blog.reverse
	end
	
	def read
		@blog = Blag.blogs.find(params[:id])
	end
	
	def new
		@blog = @user.blags.new
		keep_url_referrer
	end
	
	def edit
		@blog = @user.blags.find(params[:id])
		keep_url_referrer
	end
	
	def create
		@blog = @user.blags.create(blog_params)
		transact = @blog.save
		title = @blog.title.to_s()
		title = "<strong>"+ title +"</strong> " if !title.blank?
		respond_to do |format|
			if transact
				flash[:success] = title +"successfully created."
				format.html { redirect_to url_referrer }
				format.json { render :show, status: :created, location: url_referrer }
			else
				flash[:danger] = "Unable to save "+ title +"due to "+ @blog.errors.to_a.join(", ") +"."
				format.html { render :new }
				format.json { render json: @blog.errors, status: :unprocessable_entity }
			end
		end
	end
	
	def update
		@blog = @user.blags.find(params[:id])
		transact = @blog.update(blog_params)
		title = @blog.title.to_s()
		title = "<strong>"+ title +"</strong> " if !title.blank?
		respond_to do |format|
			if transact
				flash[:success] = title +"successfully updated."
				format.html { redirect_to url_referrer }
				format.json { render :show, status: :ok, location: url_referrer }
			else
				flash[:danger] = "Unable to update "+ title +"due to "+ @blog.errors.to_a.join(", ") +"."
				format.html { render :edit }
				format.json { render json: @blog.errors, status: :unprocessable_entity }
			end
		end
	end
	
	def destroy
		keep_url_referrer
		@blog = @user.blags.find(params[:id])
		transact = @blog.destroy
		title = @blog.title.to_s()
		title = "<strong>"+ title +"</strong> " if !title.blank?
		respond_to do |format|
			if transact
				flash[:success] = title +"successfully deleted."
				format.html { redirect_to url_referrer }
				format.json { render :show, status: :ok, location: url_referrer }
			else
				flash[:danger] = "Unable to delete "+ title +"due to "+ @blog.errors.to_a.join(", ") +"."
				format.html { render :edit }
				format.json { render json: @blog.errors, status: :unprocessable_entity }
			end
		end
	end
end