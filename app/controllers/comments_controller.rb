class CommentsController < ApplicationController
	before_filter :get_blog
	before_filter :can_delete?, only: :destroy
	
	private def comment_params
		params.require(:comment).permit(:commentor, :remarks)
	end
	
	def get_blog
		@blog = Blag.find(params[:blog_id])
	end
	
	def can_delete?
		username = current_user.email.to_s()
		if is_owner? && is_commentor?
			keep_url_referrer
			respond_to do |format|
				flash[:danger] = "Unable to delete comment due to User <strong>#{username}</strong> not authorize, only blog owner or commentor can delete the comment"
				format.html { redirect_to url_referrer }
				format.json { render json: flash[:danger], status: :unprocessable_entity }
			end
			return false
		end
		true
	end
	
	def is_owner?
		@blog && current_user.id != @blog.user_id
	end
	
	def is_commentor?
		username = current_user.email.to_s()
		@blog && username.downcase != @blog.comments.find(params[:id]).commentor.downcase
	end
	
	def create
		@comment = @blog.comments.create(comment_params)
		keep_url_referrer
		respond_to do |format|
			commentor = @comment[:commentor]
			unless commentor.blank?
				commentor = @comment[:commentor].end_with?("s") ? "'" : "'s"
				commentor = "<strong>"+ @comment[:commentor].to_s() +"</strong> "
			end
			if !@comment.errors.any?
				flash[:success] = commentor +"comment successfully posted."
				format.html { redirect_to url_referrer }
				format.json { render :show, status: :created, location: url_referrer }
			else
				flash[:danger] = commentor.titleize() +"comment unable to post due to "+ @comment.errors.to_a.join(", ") +"."
				format.html { redirect_to url_referrer }
				format.json { render json: @comment.errors, status: :unprocessable_entity }
			end
		end
	end
	
	def destroy
		begin
			keep_url_referrer
			@comment = @blog.comments.find(params[:id])
			respond_to do |format|
				commentor = @comment[:commentor]
				unless commentor.blank?
					commentor = @comment[:commentor].end_with?("s") ? "'" : "'s"
					commentor = "<strong>"+ @comment[:commentor].to_s() +"</strong> "
				end
				if @comment.destroy
					flash[:success] = commentor +"comment successfully deleted."
					format.html { redirect_to url_referrer }
					format.json { render :show, status: :ok, location: url_referrer }
				else
					flash[:danger] = commentor.titleize() +"comment unable to delete due to "+ @comment.errors.to_a.join(", ") +"."
					format.html { redirect_to url_referrer }
					format.json { render json: @comment.errors, status: :unprocessable_entity }
				end
			end
		rescue => errors
			respond_to do |format|
				flash[:danger] = "Unable to delete comment due to <strong>"+ errors.to_s() +"</strong>."
				format.html { redirect_to url_referrer }
				format.json { render json: errors, status: :unprocessable_entity }
			end
		end
	end
end