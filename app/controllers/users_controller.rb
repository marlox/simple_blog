class UsersController < ApplicationController
	skip_before_action :require_login, except: :index
	
	private def users_params
		params.require(:user).permit(:email, :password, :password_confirmation)
	end
	
	def index
		@user = current_user
	end
	
	def new
		redirect_to users_path unless current_user_session.blank?
		@user = User.new
	end
	
	def create
		@user = User.new(users_params)
		transact = @user.save
		email = @user.email.to_s()
		email = "<strong>"+ email +"</strong> user account " unless email.blank?
		respond_to do |format|
			if transact
				flash[:success] = email +"successfully created."
				format.html { redirect_to root_path }
				format.json { render :show, status: :created, location: root_path }
				current_user_session.destroy
			else
				flash[:danger] = "Unable to save "+ email +"due to "+ @user.errors.to_a.join(", ") +"."
				format.html { redirect_to new_user_path }
				format.json { render json: @user.errors, status: :unprocessable_entity }
			end
		end
	end
end