class ApplicationController < ActionController::Base
	before_action :require_login
	
	# Prevent CSRF attacks by raising an exception.
	# For APIs, you may want to use :null_session instead.
	protect_from_forgery with: :exception
	
	private def require_login
		redirect_to blags_path if params[:id] && !is_numeric?(params[:id]) && !params[:id].to_s.eql?("all")
		if current_user_session && current_user_session.stale?
			current_user_session.destroy
			respond_to do |format|
				flash[:danger] = "Session timed out!"
				format.html { nil }
				format.json { render json: flash[:danger], status: :unprocessable_entity }
			end
			redirect_to root_path
		end
		if current_user_session.blank?
			if !params[:controller].eql?("welcome")
				respond_to do |format|
					flash[:danger] = "Login required! Get Started &raquo; <span class=\"glyphicon glyphicon-user\"></span> User Options"
					format.html { nil }
					format.json { render json: flash[:danger], status: :unprocessable_entity }
				end
				redirect_to root_path
			end
		end
	end
	
	def is_numeric? value
		true if Float(value) rescue false
	end
	
	# Referrer
	def keep_url_referrer
		session[:url_referrer] = request.referer
	end
	
	def url_referrer
		url_referrer = session[:url_referrer]
		session.delete(:url_referrer)
		url_referrer
	end
	#/Referrer
	
	# User Features
	private def current_user_session
		return @current_user_session if defined?(@current_user_session)
		@current_user_session = UserSession.find
	end
	
	def current_user
		return @current_user if defined?(@current_user)
		@current_user = current_user_session && current_user_session.user
	end
	#/User
	
	helper_method :current_user_session, :current_user
end