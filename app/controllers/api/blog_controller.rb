module API
	class BlogController < ApplicationController
		skip_before_action :require_login
		
		def index
			search = params[:search]
			blog = Blag.created_asc
			blog = blog.search_blog(search) if search
			blog = blog.reverse
			render json: blog
		end
		
		def show
			blog = Blag.find(params[:id])
			render json: blog if stale?(last_modified: blog.updated_at, public: true)
		rescue
			render json: blog
		end
	end
end