class UserSessionsController < ApplicationController
	skip_before_action :require_login
	
	private def user_session_params
		params.require(:user_session).permit(:email, :password, :remember_me)
	end
	
	def new
		redirect_to users_path unless current_user.blank?
		@user_session = UserSession.new
	end
	
	def create
		@user_session = UserSession.new(user_session_params)
		transact = @user_session.save
		strEmail = @user_session.email.to_s()
		email = " <strong>"+ strEmail +"</strong>" if !strEmail.blank?
		respond_to do |format|
			if transact
				flash[:success] = "Welcome back, <strong>"+ strEmail.split("@").first.to_s() +"</strong>!"
				format.html { redirect_to users_path }
				format.json { render :show, status: :created, location: users_path }
			else
				flash[:danger] = "Unable to sign in"+ email +" because "+ @user_session.errors.to_a.join(", ") +"."
				format.html { redirect_to sign_in_path }
				format.json { render json: @user_session.errors, status: :unprocessable_entity }
			end
		end
	end
	
	def destroy
		strEmail = current_user.email.to_s()
		email = " <strong>"+ strEmail +"</strong>" if !strEmail.blank?
		transact = current_user_session.destroy
		respond_to do |format|
			if transact
				flash[:success] = "Goodbye, <strong>"+ strEmail.split("@").first.to_s() +"</strong>!"
				format.html { redirect_to root_path }
				format.json { render :show, status: :ok, location: root_path }
			else
				flash[:danger] = "Unable to sign out"+ email +" because "+ current_user_session.errors.to_a.join(", ") +"."
				format.html { redirect_to root_path }
				format.json { render json: current_user_session.errors, status: :unprocessable_entity }
			end
		end
	end
end